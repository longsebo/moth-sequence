package com.moth.cloudsequence;

/**
 * 
 */

import java.io.File;
import java.sql.Date;
import java.util.List;

import com.moth.ISystemConstant;
import com.moth.utils.ConvertHelper;
import com.moth.utils.FileOperateHelper;

/**
 * 测试文件序列并发 作者:龙色波 日期:2013-12-23
 */
public class FileSequenceTestBatchSeqVal {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		int threadnum = 1000;
		if (args.length > 0) {
			threadnum = ConvertHelper.toInt(args[0]);
		}
		String workPath = "";
		String serverPath = "";
		String sequencename = "";
		if (args.length > 1) {
			workPath = args[1];
		}
		if (args.length > 2) {
			serverPath = args[2];
			if (!serverPath.endsWith(File.separator)) {
				serverPath = serverPath + File.separator;
			}
		}
		if (args.length > 3) {
			sequencename = args[3];
		}
		CloudSequenceServer.start(10, workPath, serverPath);
		
		long begintime, endtime;
		begintime = System.currentTimeMillis();
		System.out.println("threadNum:"+threadnum);
		Date d = new Date(System.currentTimeMillis());
		List<String> seqValues;
		//for (int i = 0; i < threadnum; i++) {
			seqValues = CloudSequenceServer.getBatchNextSeqVal(threadnum, sequencename, d,d,d);
		//}		
		endtime = System.currentTimeMillis();
		System.out.println("--Sum cost time:" + (endtime - begintime));
		System.out.println("seqValues size:"+seqValues.size());
		CloudSequenceServer.stop();
		writeToSql(workPath,seqValues);
	}
	/**
	 * 写入sql文件
	 * @param seqValues
	 * @throws Exception 
	 */
	private static void writeToSql(String path,List<String> seqValues) throws Exception {
		if (!path.endsWith(File.separator)) {
			path = path + File.separator;
		}
		String logFileName = path + "insert.sql";
		for(String seqVal:seqValues){
			String msg = "insert into test(id) values('"
				+ seqVal + "');\r\n";
			FileOperateHelper.writeStringToFile(new File(logFileName),
				msg, ISystemConstant.UTF_8_ENCODE, true);
		}
	}

	
}


