package com.moth.cloudsequence;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class TestXStram {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testToXMLObject() {
		SequenceFormatBean bean;
		bean = new SequenceFormatBean();
		bean.setCycleType("0");
		bean.setRemark("订单");
		bean.setSequenceLen(10);
		bean.setSequenceRule("%d");
		bean.setSequenceName("testSequence");
		List<SequenceFormatBean> beans = new ArrayList<SequenceFormatBean>(); 
		XStream xstream = new XStream();
		xstream.alias("SequenceFormat", SequenceFormatBean.class);
		String xml = xstream.toXML(bean);
		System.out.println(xml);
	}

	@Test
	public void testFromXMLInputStream() {
		fail("Not yet implemented");
	}
	@Test
	public void testFromXMLString() {
		String xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?> <SequenceFormat>"+
				   "<cycleType>0</cycleType>"+
				   "<sequenceRule>%d</sequenceRule>"+
				   "<sequenceLen>10</sequenceLen>"+
				   "<remark>订单</remark>"+
				   "<sequenceName>testSequence</sequenceName>"+
				   "</SequenceFormat>";
		
		SequenceFormatBean bean;
		XStream xstream = new XStream(  new DomDriver());
		xstream.alias("SequenceFormat", SequenceFormatBean.class);
		bean = (SequenceFormatBean) xstream.fromXML(xml);
		System.out.println("remark:"+bean.getRemark());
	}
}
