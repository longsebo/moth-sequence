/**
 * 
 */
package com.moth.cloudsequence;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.mdfs.lang.MDFSBoolean;
import com.moth.ISystemConstant;
import com.moth.MothException;
import com.moth.utils.ConvertHelper;
import com.moth.utils.FileOperateHelper;
import com.moth.utils.StringHelper;

/**
 * 序列产生server 作者:龙色波 日期:2013-12-25
 */
/**
 * 序列值线程
 */
class SequenceVallThread extends Thread  {
    
	private String sequenceName;
	private Object[] params;
	private String seqValue=null;
	public SequenceVallThread(String sequenceName, Object[] params) {
        this.sequenceName = sequenceName;
        this.params = params;
    }

    public void run() {
    	try {
			seqValue = CloudSequenceServer.getNextSeqVal(sequenceName, params);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

	public String getSeqValue() {
		return seqValue;
	}
}
public class CloudSequenceServer {
	private static Map<String, CloudSequenceThread> queueThreads = new HashMap<String, CloudSequenceThread>();
	private static List<CloudSequenceThread> threads = new ArrayList<CloudSequenceThread>();
	/**
	 * 尚未使用序列文件锁 以序列名称为key。
	 */
	private static Map<String, Object> unuseFileLock = new HashMap<String, Object>();
	/**
	 * 标识启动标志
	 */
	private static MDFSBoolean bstart = new MDFSBoolean(false);
	/**
	 * 工作目录
	 */
	private static String workPath;
	/**
	 * 服务器的共享目录:存放服务器序列规则文件目录
	 */
	private static String serverPath;

	

	/**
	 * 启动
	 * 
	 * @param threadNum
	 *            启动管理序列的线程数
	 * @param workPath
	 *            本地工作目录
	 * @param serverPath
	 *            存放序列值的服务器目录 可以为共享目录
	 * @throws MothException
	 */
	public static void start(int threadNum, String workPath, String serverPath)
			throws MothException {
		// 校验是否重复启动
		synchronized (bstart) {
			if (!bstart.isValue()) {
				bstart.setValue(true);
			} else {
				return;
			}
		}
		
		CloudSequenceServer.workPath = workPath;
		CloudSequenceServer.serverPath = serverPath;
		// 如果工作目录不存在，则创建
		if (!FileOperateHelper.isExists(CloudSequenceServer.workPath)) {
			FileOperateHelper.newFolder(CloudSequenceServer.workPath);
		}
		//创建序列服务锁文件
		// 清理各个序列目录下的临时文件
		clearSequencePath();
		//清理服务器各序列目录下的.lck
		clearServerSequencePath(CloudSequenceServer.serverPath);
		for (int i = 0; i < threadNum; i++) {
			CloudSequenceThread thread = new CloudSequenceThread(
					new SequenceRequestNotifyBean(),
					CloudSequenceServer.serverPath);
			threads.add(thread);
			thread.start();
		}
		
	}
	/**
	 * 清理服务器各序列目录下的锁文件(.lck),服务器意外终止时，.lck可能没删除
	 * @param serverPath 服务器目录
	 * @throws MothException 
	 */
	private static void clearServerSequencePath(String serverPath) throws MothException {
		// 获取序列目录
		List<String> seqPaths;
		seqPaths = FileOperateHelper.getSubPath(serverPath);
		String extensions[]=new String[1];
		
		extensions[0] = ISystemConstant.LOCK_SUFFIX.substring(1);
		//只搜索.lck文件
		if (seqPaths != null && seqPaths.size() > 0) {
			for (String seqPath : seqPaths) {
				// 获取文件列表
				List<File> files = new ArrayList<File>();
				files.addAll(FileOperateHelper.listFiles(new File(seqPath),
						extensions, false));
				// 尝试删除锁文件,删除失败则认为锁文件在用
				for(File file:files){
					String fileName = file.getAbsolutePath();
					// 删除锁文件
					if (FileOperateHelper.isExists(fileName)) {
						try {
							FileOperateHelper.delFile(fileName);
						} catch (MothException e) {
							System.out.println("Retry delFile:" + fileName+" fail! Possible is using.");
						}
					}
				}
			}
		}
		
	}
	/**
	 * 构造序列锁文件
	 * @param serverPath 服务器目录
	 * @return
	 */
	private static String makeSequenceLockFileName(String serverPath) {
		String sequenceLockFileName;
		if(serverPath.endsWith(File.separator)){
			sequenceLockFileName = serverPath+ISystemConstant.SEQUENCE_SVR_LOCK_FILE_NAME;
		}else{
			sequenceLockFileName = serverPath+File.separator+ISystemConstant.SEQUENCE_SVR_LOCK_FILE_NAME;
		}
		return sequenceLockFileName;
	}

	/**
	 * 清理各个序列目录下的临时文件: 1.检索.res 文件列表， .res文件更名为.unuse。其他非.seq非.unuse文件删除
	 * 
	 * @throws MothException
	 * 
	 */
	private static void clearSequencePath() throws MothException {
		// 获取序列目录
		List<String> seqPaths;
		seqPaths = FileOperateHelper.getSubPath(workPath);
		// 最大区间值
		long maxSegVal = 0;
		// 最大尚未使用的值
		long maxNoUseVal = 0;
		//标志存在.unuse或无后缀文件
		boolean existTemp = false;
		if (seqPaths != null && seqPaths.size() > 0) {
			for (String seqPath : seqPaths) {
				existTemp = false;
				// 获取文件列表
				List<File> files = new ArrayList<File>();
				files.addAll(FileOperateHelper.listFiles(new File(seqPath),
						null, false));
				// 检索.seq区间最大值文件
				Collection<File> seqFiles;
				seqFiles = FileOperateHelper.listFiles(new File(seqPath),
						new String[] { ISystemConstant.SEQUENCE_SUFFIX
								.substring(1) }, false);
				if (seqFiles != null && seqFiles.size() > 0) {
					String line;
					line = FileOperateHelper.readFileToString(seqFiles
							.iterator().next(), ISystemConstant.UTF_8_ENCODE);
					line = StringHelper.trimCrlf(line);
					try {
						maxSegVal = ConvertHelper.toLong(line);
					} catch (Exception e) {
						e.printStackTrace();
						throw new MothException(e);
					}
				}else{
					continue;
				}
				if (files.size() > 0) {
					long tmpSeqVal;
					for (File file : files) {
						String fileName = file.getAbsolutePath();

						// 不带后缀的文件
						if (fileName.indexOf(".") == -1) {
							if(!existTemp){
								existTemp = true;
							}
							// 其他不带后缀的有内容的话，是来不及修改文件名后缀，但是已经取得序列值，也要加入改名和更新序列值
							String line;
							line = FileOperateHelper.readFileToString(file,
									ISystemConstant.UTF_8_ENCODE);
							// 检查行是否写入完整
							if (line.endsWith("\r\n")) {
								line = StringHelper.trimCrlf(line);
								try {
									tmpSeqVal = ConvertHelper.toLong(line);
								} catch (Exception e1) {
									e1.printStackTrace();
									continue;
								}
								if(tmpSeqVal>maxNoUseVal){
									maxNoUseVal = tmpSeqVal;
								}
								try {
									// 改名文件
									renameToUnuse(fileName);
									// 构造未用序列文件锁
									makeUnuseFileLock(seqPath);
								} catch (Exception e) {
									e.printStackTrace();
								}

							} else {
								//直接删除，重新生成
								FileOperateHelper.delFile(fileName);
							}
						} else if (fileName
								.endsWith(ISystemConstant.SEQUENCE_UNUSE_SUFFIX)) {
							if(!existTemp){
								existTemp = true;
							}
							String line;
							line = FileOperateHelper.readFileToString(file,
									ISystemConstant.UTF_8_ENCODE);
							line = StringHelper.trimCrlf(line);
							try {
								tmpSeqVal = ConvertHelper.toLong(line);
							} catch (Exception e1) {
								e1.printStackTrace();
								continue;
							}
							if(tmpSeqVal>maxNoUseVal){
								maxNoUseVal = tmpSeqVal;
							}
							// 构造未用序列文件锁
							makeUnuseFileLock(seqPath);
						} else if(!fileName.endsWith(ISystemConstant.SEQUENCE_SUFFIX)){
							FileOperateHelper.delFile(fileName);
						}
					}
					//如果存在临时文件
					if(existTemp){
					 for (maxNoUseVal=maxNoUseVal+1;maxNoUseVal<=maxSegVal;maxNoUseVal++) {
						String fileName = seqPath+File.separator+getUUID()+ISystemConstant.SEQUENCE_UNUSE_SUFFIX;
						// 不带后缀的文件
						try {
							FileOperateHelper.writeStringToFile(new File(fileName), ConvertHelper.toString(maxNoUseVal)+"\r\n", ISystemConstant.UTF_8_ENCODE);
							// 构造未用序列文件锁
							makeUnuseFileLock(seqPath);
						} catch (Exception e) {
							e.printStackTrace();
							throw new MothException(e);
						}
					}
				  }
				}
			}
		}
	}

	private static void makeUnuseFileLock(String seqPath) {
		if (!unuseFileLock.containsKey(seqPath + File.separator)) {
			unuseFileLock.put(seqPath + File.separator, new Object());
		}
	}

	/**
	 * 把.res 更名为 .unuse
	 * 
	 * @param fileName
	 *            .res文件名
	 * @throws MothException
	 */
	private static void renameToUnuse(String fileName) throws MothException {
		String unuseFileName;
		String simpleFileName;

		simpleFileName = FileOperateHelper.getFileName(fileName);
		int pos;
		pos = simpleFileName
				.lastIndexOf(ISystemConstant.SEQUENCE_RESPONSE_SUFFIX);
		if (pos >= 0) {
			simpleFileName = simpleFileName.substring(0, pos);
		}
		String path;
		path = FileOperateHelper.getPath(fileName);
		unuseFileName = path + File.separator + simpleFileName
				+ ISystemConstant.SEQUENCE_UNUSE_SUFFIX;
		FileOperateHelper.rename(fileName, unuseFileName);
	}

	public static void stop() {
		for (CloudSequenceThread thread : threads) {
			thread.setExist(true);
		}
		// 等待线程退出
		while (!threads.isEmpty()) {
			CloudSequenceThread thread;
			for (int i = threads.size() - 1; i >= 0; i--) {
				if (i < threads.size()) {
					thread = threads.get(i);
					if (!thread.isAlive()) {
						threads.remove(i);
					}
				}
			}
			try {
				Thread.sleep(ISystemConstant.THREAD_WAIT_SLEEP_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 获取下一个序列值
	 * 
	 * @param seqName
	 *            序列名称
	 * @param prefix
	 *            前缀
	 * @param len
	 *            长度  是指序列号格式化后长度，不包含前后缀
	 * @param suffix 后缀
	 * @return
	 * @throws Exception
	 */
//	public static String getNextSeqVal(String seqName, String prefix,String suffix, int len)
//			throws Exception {
//		// 1.获取队列名称对应线程
//		CloudSequenceThread thread;
//		String normalFileName;
//		if (StringHelper.isEmpty(workPath)) {
//			throw new Exception("workPath is empty");
//		}
//		String subPath;
//		if (!workPath.endsWith(File.separator)) {
//			subPath = workPath + File.separator + seqName + File.separator;
//		} else {
//			subPath = workPath + seqName + File.separator;
//		}
//		// 如果子目录不存在，则创建
//		if (!FileOperateHelper.isExists(subPath)) {
//			FileOperateHelper.newFolder(subPath);
//		}
//		// 如果存在.unuse文件，则尝试取.unuse文件
//		String unUseSeqVal = null;
//		boolean haveunuseSeq = false;
//		synchronized (unuseFileLock) {
//			haveunuseSeq = unuseFileLock.containsKey(subPath);
//		}
//
//		if (haveunuseSeq) {
//			unUseSeqVal = getSeqFromUnUseFile(subPath);
//		}
//		// 如果不为空，则返回
//		if (!StringHelper.isEmpty(unUseSeqVal)) {
//			unUseSeqVal = formatSeqVal(len, unUseSeqVal);
//			return (prefix==null?"":prefix) +  unUseSeqVal+(suffix==null?"":suffix);
//		}
//		normalFileName = subPath + seqName + ISystemConstant.SEQUENCE_SUFFIX;
//		synchronized (queueThreads) {
//			thread = queueThreads.get(seqName);
//		}
//		// 2.如果没有找到，则获取请求数最少的线程
//		if (thread == null) {
//			thread = getMinSequenceThread();
//			// 增加线程管理序列个数
//			thread.setSequenceSize(thread.getSequenceSize() + 1);
//			queueThreads.put(seqName, thread);
//		}
//		// 3.获取线程的同步对象
//		SequenceItem item = new SequenceItem();
//		item.setSequenceFileName(normalFileName);
//		item.setSequenceValFileName(makeSeqValFileName(normalFileName));
//		item.setSequenName(seqName);
//		int size;
//		int sleeptime = ISystemConstant.THREAD_WAIT_SLEEP_TIME;
//		while (true) {
//			synchronized (thread.getNotifyBean()) {
//				size = thread.getNotifyBean().getSize();
//			}
//			// 如果队列大于1000,则睡眠等待
//			if (size > 1000) {
//				if (sleeptime > 2000) {
//					sleeptime = ISystemConstant.THREAD_WAIT_SLEEP_TIME;
//				} else {
//					sleeptime = sleeptime + 1;
//				}
//				Thread.sleep(sleeptime);
//			} else {
//				break;
//			}
//
//		}
//		synchronized (thread.getNotifyBean()) {
//			thread.getNotifyBean().add(item);
//			thread.getNotifyBean().notify();
//		}
//		// 创建保存序列值的文件
//		FileOperateHelper.newFile(item.getSequenceValFileName(), null);
//		String sequenceValResFileName = item.getSequenceValFileName();
//		// 6.等待通知
//		sleeptime = ISystemConstant.THREAD_WAIT_SLEEP_TIME;
//		while (true) {
//			// System.out.println("synchronized (item:"+item.getSequenceValFileName()+")");
//			synchronized (item) {
//				try {
//					item.wait(1000);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//					break;
//				}
//				// 确认文件名已保存
//				if (item.isFinish()) {
//					break;
//				}
//			}
//
//			// System.out.println("睡眠一下，以释放item:"+item.getSequenceValFileName()+")");
//			// 睡眠一下，以释放item
//			if (sleeptime > 2000) {
//				sleeptime = ISystemConstant.THREAD_WAIT_SLEEP_TIME;
//			} else {
//				sleeptime = sleeptime + 1;
//			}
//			Thread.sleep(sleeptime);
//		}
//		// 7.取出序列值内容，格式化返回
//		String line;
//		int waittime = ISystemConstant.THREAD_WAIT_SLEEP_TIME;
//		while (true) {
//			try {
//				line = FileOperateHelper.readFileToString(new File(
//						sequenceValResFileName), ISystemConstant.UTF_8_ENCODE);
//				break;
//			} catch (Exception e) {
//				// 如果一旦读发生异常，则睡眠重试
//				e.printStackTrace();
//				if (waittime < 2000) {
//					waittime = waittime + 1;
//				} else {
//					waittime = ISystemConstant.THREAD_WAIT_SLEEP_TIME;
//				}
//				Thread.sleep(waittime);
//			}
//		}
//		String seqval = StringHelper.trimCrlf(line);
//		String strId = "";
//		strId = formatSeqVal(len, seqval);
//		// 8.删除过程文件
//		FileOperateHelper.delFile(sequenceValResFileName);
//		return (prefix==null?"":prefix) +  strId+(suffix==null?"":suffix);
//	}

	private static String formatSeqVal(int len, String seqval) throws Exception {
		String strId;
		if (len <= 0) {
			strId = seqval;
		} else {
			strId = String.format("%1$0" + ConvertHelper.toString(len) + "d",
					ConvertHelper.toInt(seqval));
		}
		return strId;
	}

	/**
	 * 尝试获取未使用的序列文件
	 * 
	 * @param subPath
	 *            序列目录
	 * @param seqName
	 * @return
	 */
	private static String getSeqFromUnUseFile(String subPath) {
		Object lockObj;
		synchronized (unuseFileLock) {
			lockObj = unuseFileLock.get(subPath);
		}
		if (lockObj != null) {
			synchronized (lockObj) {
				// 获取子目录下的.unuse文件列表
				Collection<File> files = FileOperateHelper.listFiles(new File(
						subPath),
						new String[] { ISystemConstant.SEQUENCE_UNUSE_SUFFIX
								.substring(1) }, false);
				if (files != null && files.size() > 0) {
					while (files.iterator().hasNext()) {
						File f = files.iterator().next();
						String line;
						try {
							line = FileOperateHelper.readFileToString(f,
									ISystemConstant.UTF_8_ENCODE);
							FileOperateHelper.delFile(f.getAbsolutePath());
							return StringHelper.trimCrlf(line);
						} catch (MothException e) {
							e.printStackTrace();
						}
					}
					return null;
				} else {
					return null;
				}
			}
		} else {
			return null;
		}
	}

	/**
	 * 尝试获取未使用的序列文件
	 * 
	 * @param subPath
	 *            序列目录
	 *            
	 * @param num 最大序列个数
	 * @return
	 */
	private static List<String> getSeqFromUnUseFile(String subPath,int num) {
		Object lockObj;
		List<String> retList = new ArrayList<String>();
		synchronized (unuseFileLock) {
			lockObj = unuseFileLock.get(subPath);
		}
		if (lockObj != null) {
			synchronized (lockObj) {
				// 获取子目录下的.unuse文件列表
				Collection<File> files = FileOperateHelper.listFiles(new File(
						subPath),
						new String[] { ISystemConstant.SEQUENCE_UNUSE_SUFFIX
								.substring(1) }, false);
				if (files != null && files.size() > 0) {
					while (files.iterator().hasNext()) {
						File f = files.iterator().next();
						String line;
						try {
							line = FileOperateHelper.readFileToString(f,
									ISystemConstant.UTF_8_ENCODE);
							FileOperateHelper.delFile(f.getAbsolutePath());
							retList.add(StringHelper.trimCrlf(line));
							if(retList.size()>=num){
								return retList;
							}
						} catch (MothException e) {
							e.printStackTrace();
						}
					}
					return retList;
				} else {
					return retList;
				}
			}
		} else {
			return null;
		}
	}
	/**
	 * 创建序列值文件名
	 * 
	 * @param normalFileName
	 * @return
	 */
	private static String makeSeqValFileName(String normalFileName) {
		String path;
		path = FileOperateHelper.getPath(normalFileName);
		return path + File.separator + getUUID();
	}

	/**
	 * 获取序列队列个数最少的线程
	 * 
	 * @return
	 */
	private static CloudSequenceThread getMinSequenceThread() {
		CloudSequenceThread minNumThread = null;
		for (int i = 0; i < threads.size(); i++) {
			if (i == 0) {
				minNumThread = threads.get(i);
			} else {
				if (threads.get(i).getSequenceSize() < minNumThread
						.getSequenceSize()) {
					minNumThread = threads.get(i);
				}
			}
		}
		return minNumThread;
	}

	/**
	 * 获取全球唯一标识
	 * 
	 * @return
	 */
	private static String getUUID() {
		return UUID.randomUUID().toString();
	}
	
	/**
	 * 批量获取序列值
	 * @param num
	 * @param seqName
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public static List<String> getBatchNextSeqVal(int num,String seqName, Object ... params ) throws Exception{
		List<String> returnList = new ArrayList<String>();
		// 1.获取队列名称对应线程
		CloudSequenceThread thread;
		String normalFileName;
		if (StringHelper.isEmpty(workPath)) {
			throw new Exception("workPath is empty");
		}	
		//检查规则文件是否存在
		checkRuleFile(seqName);
		
		String subPath;
		if (!workPath.endsWith(File.separator)) {
			subPath = workPath + File.separator + seqName + File.separator;
		} else {
			subPath = workPath + seqName + File.separator;
		}
		// 如果子目录不存在，则创建
		if (!FileOperateHelper.isExists(subPath)) {
			FileOperateHelper.newFolder(subPath);
		}
		// 如果存在.unuse文件，则尝试取.unuse文件		
		boolean haveunuseSeq = false;
		synchronized (unuseFileLock) {
			haveunuseSeq = unuseFileLock.containsKey(subPath);
		}

		if (haveunuseSeq) {
			returnList.addAll(getSeqFromUnUseFile(subPath,num));
		}
		// 如果序号个数已足，则返回
		if (returnList.size()>=num) {
			return returnList;
		}
		normalFileName = subPath + seqName + ISystemConstant.SEQUENCE_SUFFIX;
		synchronized (queueThreads) {
			thread = queueThreads.get(seqName);
		}
		// 2.如果没有找到，则获取请求数最少的线程
		if (thread == null) {
			thread = getMinSequenceThread();
			// 增加线程管理序列个数
			thread.setSequenceSize(thread.getSequenceSize() + 1);
			queueThreads.put(seqName, thread);
		}
		List<SequenceItem> items = new ArrayList<SequenceItem>();
		List<SequenceItem> tempItems = new ArrayList<SequenceItem>();
		// 3.获取线程的同步对象
		for(int i=0;i<num - returnList.size();i++){
			SequenceItem item = new SequenceItem();
			item.setSequenceFileName(normalFileName);
			item.setSequenceValFileName(makeSeqValFileName(normalFileName));
			item.setSequenName(seqName);
			item.setParams(params);
			// 创建保存序列值的文件
			FileOperateHelper.newFile(item.getSequenceValFileName(), null);
			int size;
			while (true) {
				synchronized (thread.getNotifyBean()) {
					size = thread.getNotifyBean().getSize();
				}
				// 如果队列大于1000,则睡眠等待
				if (size > 1000) {
					thread = getMinSequenceThread();
					// 增加线程管理序列个数
					thread.setSequenceSize(thread.getSequenceSize() + 1);
					queueThreads.put(seqName, thread);
//					if (sleeptime > 2000) {
//						sleeptime = ISystemConstant.THREAD_WAIT_SLEEP_TIME;
//					} else {
//						sleeptime = sleeptime + 1;
//					}
//					Thread.sleep(sleeptime);
				} else {
					break;
				}
	
			}
			tempItems.add(item);
			if(tempItems.size()>=1000){
				synchronized (thread.getNotifyBean()) {
					thread.getNotifyBean().addAll(tempItems);
					thread.getNotifyBean().notify();
					items.addAll(tempItems);
					tempItems.clear();
				}
			}
		}
		if(tempItems.size()>=1){
			synchronized (thread.getNotifyBean()) {
				thread.getNotifyBean().addAll(tempItems);
				thread.getNotifyBean().notify();
				items.addAll(tempItems);
				tempItems.clear();
			}
		}
		int waittime = ISystemConstant.THREAD_WAIT_SLEEP_TIME;
		while(returnList.size()<num && items.size()>0){
			for(int j=items.size()-1;j>=0;j--){
				SequenceItem item = items.get(j);
				String sequenceValResFileName = item.getSequenceValFileName();
				synchronized (item) {					
					// 确认文件名已保存
					if (item.isFinish()) {
						// 7.取出序列值内容返回
						String line;
						
						while (true) {
							try {
								line = FileOperateHelper.readFileToString(new File(
										sequenceValResFileName), ISystemConstant.UTF_8_ENCODE);
								break;
							} catch (Exception e) {
								// 如果一旦读发生异常，则睡眠重试
								e.printStackTrace();
								if (waittime < 2000) {
									waittime = waittime + 1;
								} else {
									waittime = ISystemConstant.THREAD_WAIT_SLEEP_TIME;
								}
								Thread.sleep(waittime);
							}
						}
						String seqval = StringHelper.trimCrlf(line);
						returnList.add(seqval);
						// 8.删除过程文件
						FileOperateHelper.delFile(sequenceValResFileName);
						//从队列里删除
						items.remove(j);
					}
				}
			}
			if (waittime < 2000) {
				waittime = waittime + 1;
			} else {
				waittime = ISystemConstant.THREAD_WAIT_SLEEP_TIME;
			}
			Thread.sleep(waittime);
			System.out.println("waittime:"+waittime);
		}
		return returnList;
	}
	/**
	 * 获取下一个序列值
	 * 
	 * @param seqName
	 *            序列名称
	 * @param params
	 *            动态参数
	 * @return
	 * @throws Exception 
	 * @throws Exception
	 */
	public static String getNextSeqVal(String seqName, Object ... params ) throws Exception{
		List<String> seqVals = null;
		seqVals = getBatchNextSeqVal(1, seqName, params);
		if(seqVals!=null && seqVals.size()>0){
		  return seqVals.get(0);		
		}else{
		  return null;
		}
	}

	private static String checkRuleFile(String seqName) throws Exception {
		String ruleFileName;
		if(!serverPath.endsWith(File.separator)){
			ruleFileName = serverPath+File.separator+seqName+ISystemConstant.SEQUENCE_SUFFIX;
		}else{
			ruleFileName = serverPath+seqName+ISystemConstant.SEQUENCE_SUFFIX;
		}
		if(!FileOperateHelper.isExists(ruleFileName)){
			throw new Exception("规则文件:"+ruleFileName+"不存在!");
		}
		return ruleFileName;
	}
}
