/**
 * 
 */
package com.moth.cloudsequence;

/**
 * 序列项目
 * 作者:龙色波
 * 日期:2013-12-25
 */
public class SequenceItem {
	/**
	 * 序列文件名
	 */
	private String sequenceFileName;
	/**
	 * 接收序列值文件名
	 */
	private String sequenceValFileName;
	/**
	 * 标志取序列值完成
	 */
	private boolean finish = false;
	/**
	 * 队列名称
	 */
	private String sequenName;
	/**
	 * 动态参数
	 */
	private Object[] params;
	public SequenceItem(){
		
	}
	public SequenceItem(String sequenceFileName){
		this.sequenceFileName = sequenceFileName;
	}
	public String getSequenceFileName() {
		return sequenceFileName;
	}

	public void setSequenceFileName(String sequenceFileName) {
		this.sequenceFileName = sequenceFileName;
	}

	public String getSequenceValFileName() {
		return sequenceValFileName;
	}
	public void setSequenceValFileName(String sequenceValFileName) {
		this.sequenceValFileName = sequenceValFileName;
	}
	public boolean isFinish() {
		return finish;
	}
	public void setFinish(boolean finish) {
		this.finish = finish;
	}
	public String getSequenName() {
		return sequenName;
	}
	public void setSequenName(String sequenName) {
		this.sequenName = sequenName;
	}
	public void setParams(Object[] params) {
		this.params = params;
	}
	public Object[] getParams() {
		return params;
	}
}
