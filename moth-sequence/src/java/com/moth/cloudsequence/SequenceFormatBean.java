/**
 * 
 */
package com.moth.cloudsequence;

import java.sql.Timestamp;

/**
 * 序列格式化Bean
 * 作者:龙色波
 * 日期:2014-10-20
 */
public class SequenceFormatBean {
	/**
	 * 周期类型
	 * 0=无周期,
	 * 1=年,
	 * 2=月,
	 * 3=日
	 */
	private String cycleType;
	/**
	 * 序列格式规则
	 */
	private String sequenceRule;
	/**
	 * 序列值长度
	 */
	private Integer sequenceLen;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 序列名称
	 */
	private String sequenceName;
	/**
	 * 更新日期
	 */
	private Timestamp updateDate;
	/**
	 * 当前格式化后的序列值
	 */
	private String currentFmtValue;
	/**
	 * 当前序列值
	 */
	private Long currentValue;
	public String getCycleType() {
		return cycleType;
	}
	public void setCycleType(String cycleType) {
		this.cycleType = cycleType;
	}
	public String getSequenceRule() {
		return sequenceRule;
	}
	public void setSequenceRule(String sequenceRule) {
		this.sequenceRule = sequenceRule;
	}
	public Integer getSequenceLen() {
		return sequenceLen;
	}
	public void setSequenceLen(Integer sequenceLen) {
		this.sequenceLen = sequenceLen;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSequenceName() {
		return sequenceName;
	}
	public void setSequenceName(String sequenceName) {
		this.sequenceName = sequenceName;
	}
	
	public String getCurrentFmtValue() {
		return currentFmtValue;
	}
	public void setCurrentFmtValue(String currentFmtValue) {
		this.currentFmtValue = currentFmtValue;
	}
	public Long getCurrentValue() {
		return currentValue;
	}
	public void setCurrentValue(Long currentValue) {
		this.currentValue = currentValue;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	
}
