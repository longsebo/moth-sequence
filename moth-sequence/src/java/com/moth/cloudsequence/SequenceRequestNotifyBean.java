/**
 * 
 */
package com.moth.cloudsequence;

import java.util.ArrayList;
import java.util.List;

/**
 * 序列请求bean
 * 作者:龙色波
 * 日期:2013-12-25
 */
public class SequenceRequestNotifyBean {
	private List<SequenceItem> items = new ArrayList<SequenceItem>();
	
	public  void  add(SequenceItem item){
		items.add(item);
	}
	public  void addAll(List<SequenceItem> items){
		this.items.addAll(items);
	}
	public  List<SequenceItem> removeAll(){
		List<SequenceItem> newItems = new ArrayList<SequenceItem>();
		newItems.addAll(items);
		items.clear();
		return newItems;
	}
	public   int getSize(){
		if(items!=null){
			return items.size();
		}else{
			return 0;
		}
	}
}
